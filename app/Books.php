<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Books extends Model
{
    protected $table = 'books';

    public $timestamps = false;

     protected $fillable= array('name','return_time','user_id');


    public function user() {
        return $this->belongsTo('App\Users');
    }
}
