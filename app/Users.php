<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Users extends Model
{
    protected $table = 'users';

    public $timestamps = false;

    protected $fillable= array('name');

	public function books(){
		return $this->hasMany('App\Books');
	}




}
