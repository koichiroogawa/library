<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<link rel="apple-touch-icon" sizes="76x76" href="assets/img/apple-icon.png">
	<link rel="icon" type="image/png" sizes="96x96" href="assets/img/favicon.png">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<title>XX Library</title>

	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />

    <!-- Bootstrap core CSS     -->
    <link href="{{asset('/css/bootstrap.min.css')}}" rel="stylesheet" />
    <!-- Animation library for notifications   -->
    <link href="{{asset('/css/animate.min.css')}}" rel="stylesheet"/>
    <!--  Paper Dashboard core CSS    -->
    <link href="{{asset('/css/paper-dashboard.css')}}" rel="stylesheet"/>
    <link href="{{asset('/css/library.css')}}" rel="stylesheet" />
    <!--  Fonts and icons     -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Muli:400,300' rel='stylesheet' type='text/css'>
    <link href="assets/css/themify-icons.css" rel="stylesheet">

</head>
<body>

<div class="wrapper">
	<div class="sidebar" data-background-color="white" data-active-color="danger">
    <!--
		Tip 1: you can change the color of the sidebar's background using: data-background-color="white | black"
		Tip 2: you can change the color of the active button using the data-active-color="primary | info | success | warning | danger"
	-->
    	<div class="sidebar-wrapper">
            <div class="logo">
                <a href="library.html" class="simple-text">
                   XX  Library
                </a>
            </div>
            <ul class="nav">
                <li class="link disabled">
                    <a>
                        <i class="ti-book"></i>
                        <p>Book List</p>
                    </a>
                </li>
               <!--  <li>
                    <a class="link" id="link-book-list">
                        <i class="ti-view-list-alt"></i>
                        <p>Borrow Book List</p>
                    </a>
                </li> -->
                <li class="link disabled" id="link-book-list" class="active">
                    <a href="library.html">
                        <i class="ti-user"></i>
                        <p>Borrow History</p>
                    </a>
                </li>
                <li class="link disabled">
                    <a href="login.html">
                        <i class="ti-back-left"></i>
                        <p>Logout</p>
                    </a>
                </li>
            </ul>
    	</div>
    </div>

    <div class="main-panel">
		<nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle">
                      <!--   <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar bar1"></span>
                        <span class="icon-bar bar2"></span>
                        <span class="icon-bar bar3"></span> -->
                    </button>
                    <span class="navbar-brand" id="page-title">Login</span>
                </div>
            </div>
        </nav>
        <!-- <div id="error-alert"class="alert alert-danger" style="display: none">
            <span class="close">×</span>
            <strong>ERROR!</strong> API ERROR.
        </div>
        <div id="success-alert"class="alert alert-success" style="display: none">
            <span class="close">×</span>
            <strong>SUCCESS!</strong> You returned the book.
        </div> -->
        <div id="area-book-list" class="area content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <!-- <div class="header"> -->
                                <!-- <h4 class="title">Library</h4> -->
                                <!-- <p class="category">Here is a subtitle for this table</p> -->
                           <!--  </div> -->
                            <div class="content">
                                <form action="library.html">
                                    <div class="row">
                                        <div class="col-md-10">
                                            <div class="form-group">
                                                <form method="POST" action="{{ route('login') }}">
                                                @csrf
                                                <label for="exampleInputEmail1">email address</label>
                                                <input type="email" class="form-control border-input" placeholder="Email" name="email" value="{{ old('email') }}" required autofocus>
                                                @if ($errors->has('email'))
                                                    <span class="invalid-feedback">
                                                        <strong>{{ $errors->first('email') }}</strong>
                                                    </span>
                                                 @endif

                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-10">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">password</label>
                                                <input type="password" class="form-control border-input" placeholder="password" name="password" required>
                                                @if ($errors->has('password'))
                                                    <span class="invalid-feedback">
                                                        <strong>{{ $errors->first('password') }}</strong>
                                                    </span>
                                                @endif

                                            </div>
                                        </div>
                                    </div>
                                    <div class="text-center">
                                        <button type="submit" class="btn btn-info btn-fill btn-wd">Login</button>
                                    </div>
                                    <div class="clearfix"></div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <footer class="footer">
            <div class="container-fluid">
                <nav class="pull-left">
                </nav>
                <div class="copyright pull-right">
                    &copy; <script>document.write(new Date().getFullYear())</script>
            </div>
        </footer>
    </div>
</div>

</body>
    <!--   Core JS Files   -->
    <script src="assets/js/jquery-1.10.2.js" type="text/javascript"></script>
	<script src="assets/js/bootstrap.min.js" type="text/javascript"></script>
	<!--  Checkbox, Radio & Switch Plugins -->
	<script src="assets/js/bootstrap-checkbox-radio.js"></script>
    <!--  Notifications Plugin    -->
    <script src="assets/js/bootstrap-notify.js"></script>
    <!-- Paper Dashboard Core javascript and methods for Demo purpose -->
	<script src="assets/js/paper-dashboard.js"></script>
	<!-- Paper Dashboard DEMO methods, don't include it in your project! -->
	<!-- <script src="assets/js/library.js"></script> -->
</html>
